
-- SUMMARY --

N/A Checkbox will allow website builders to add a checkbox next to a number
textfield. The idea is to provide the users a textfield to enter a numeric
value, or mark N/A in case they don't have it. When the N/A option is marked, a
different value is set in the database so that it knows what was selected.

Both the internal value of the checkbox and the N/A label are configurable. The
internal value can be outside of the max, min, and allowed values and it will
still validate properly.

For a full description visit the project page:
  http://drupal.org/project/na_checkbox
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/na_checkbox


-- REQUIREMENTS --

This module depends on CCK and Number, which can be found here:
  http://drupal.org/project/cck


-- INSTALLATION --

Install as usual, see http://drupal.org/node/70151 for further information.


-- TO USE --

To use this module, go into the CCK field editing form in Administer >> Content
management >> Content types >> Manage fields, and select the field you want to
enable N/A Checkbox for.

You can now configure the value that will be stored and the label that will be
displayed.


-- CONTACT --

Author:
* Victor Kareh (vkareh) - http://www.vkareh.net
