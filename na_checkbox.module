<?php

/**
 * Implementation of hook_widget_info()
 */
function na_checkbox_widget_info() {
  return module_invoke('number', 'widget_info');
}

/**
 * Implementation of hook_widget().
 */
function na_checkbox_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  return module_invoke('number', 'widget', $form, $form_state, $field, $items, $delta);
}

/**
 * Implementation of hook_widget_settings().
 */
function na_checkbox_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      $form['na_checkbox'] = array(
        '#type' => 'fieldset',
        '#title' => t('N/A checkbox'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#weight' => 5,
      );
      $form['na_checkbox']['na_checkbox_enable'] = array(
        '#type' => 'radios',
        '#title' => t('Enable N/A checkbox'),
        '#description' => t('Select whether you want to display an N/A checkbox.'),
        '#default_value' => isset($widget['na_checkbox_enable']) ? $widget['na_checkbox_enable'] : 0,
        '#options' => array(0 => 'No', 1 => 'Yes'),
        '#weight' => -10,
      );
      $form['na_checkbox']['na_checkbox_label'] = array(
        '#type' => 'textfield',
        '#title' => t('N/A checkbox label'),
        '#description' => t('Enter the label you want for the N/A checkbox.'),
        '#default_value' => isset($widget['na_checkbox_label']) ? $widget['na_checkbox_label'] : 'N/A',
        '#weight' => -9,
      );
      $form['na_checkbox']['na_checkbox_value'] = array(
        '#type' => 'textfield',
        '#title' => t('N/A checkbox value'),
        '#description' => t('Enter the value you want to store when the N/A checkbox is selected.'),
        '#default_value' => isset($widget['na_checkbox_value']) ? $widget['na_checkbox_value'] : '',
        '#weight' => -8,
      );
      return $form;
      break;
    case 'save':
      return array('na_checkbox_enable', 'na_checkbox_value', 'na_checkbox_label');
      break;
  }
}

/**
 * Implementation of hook_field_info().
 */
function na_checkbox_field_info() {
  return module_invoke('number', 'field_info');
}

/**
 * Implementation of hook_field_settings().
 */
function na_checkbox_field_settings($op, $field) {
  return module_invoke('number', 'field_settings', $op, $field);
}

/**
 * Implementation of hook_field().
 */
function na_checkbox_field($op, &$node, $field, &$items, $teaser, $page) {
  // Only process if N/A Checkbox is enabled.
  if ($field['widget']['na_checkbox_enable'] == 1) {
    switch ($op) {
      case 'validate':
        // Hijack validation to include N/A Checkbox value as an allowed value.
        if (isset($field['allowed_values_php'])) {
          unset($field['allowed_values']);
          $field['allowed_values'] = $field['widget']['na_checkbox_value'] . "|" . $field['widget']['na_checkbox_label'];
          $allowed_values = eval($field['allowed_values_php']);
          foreach ($allowed_values as $allowed_value => $allowed_label) {
            $field['allowed_values'] .= "\n" . $allowed_value . "|" . $allowed_label;
          }
          unset($field['allowed_values_php']);
        }
        elseif (isset($field['allowed_values'])) {
          $field['allowed_values'] = $field['allowed_values'] . "\n" . $field['widget']['na_checkbox_value'] . "|" . $field['widget']['na_checkbox_label'];
        }
        if (is_array($items)) {
          foreach ($items as $delta => $item) {
            //if ($item['value'] == $item['na_checkbox']) {
            if ($item['value'] == $field['widget']['na_checkbox_value']) {
              if (is_numeric($field['min']) && $item['value'] < $field['min']) {
                $field['min'] = $field['widget']['na_checkbox_value'];
              }
              if (is_numeric($field['max']) && $item['value'] > $field['max']) {
                $field['max'] = $field['widget']['na_checkbox_value'];
              }
            }
          }
        }
        // Continue with the regular hook_field().
        return module_invoke('number', 'field', $op, $node, $field, $items, $teaser, $page);
      case 'sanitize':
        foreach ($items as $delta => $item) {
          // Display the label instead of the value.
          if ($items[$delta]['value'] == $field['widget']['na_checkbox_value']) {
            $items[$delta]['value'] = $field['widget']['na_checkbox_label'];
          }
        }
        break;
    }
  }
}

/**
 * Implementation of hook_elements().
 */
function na_checkbox_elements() {
  return array(
    'number' => array(
      '#process' => array('na_checkbox_element_process'),
      '#element_validate' => array('na_checkbox_element_validate'),
      '#na_checkbox' => FALSE,
    ),
  );
}

/**
 * Form element process function to render the checkbox element.
 */
function na_checkbox_element_process($element, $edit, &$form_state, $form) {
  // Do nothing if it's not a node editing form.
  if ($form['#id'] != 'node-form') {
    return $element;
  }
  if (empty($element['#na_checkbox']) && isset($form['#field_info'])) {
    $element['#na_checkbox'] = array(
      'na_checkbox_enable' => $form['#field_info'][$element['#field_name']]['widget']['na_checkbox_enable'],
      'na_checkbox_label' => $form['#field_info'][$element['#field_name']]['widget']['na_checkbox_label'],
      'na_checkbox_value' => $form['#field_info'][$element['#field_name']]['widget']['na_checkbox_value'],
    );
  }
  // Do nothing if na_checkbox option is not enabled.
  if ($element['#na_checkbox']['na_checkbox_enable'] < 1) {
    return $element;
  }
  // Wrap parent field.
  $element['value']['#prefix'] = '<div class="na-checkbox-parent">' . $element['value']['#prefix'];
  $element['value']['#suffix'] = $element['value']['#suffix'] . '</div>';
  // If parent field is required, allow marking N/A as a valid option. @see Take care of this in hook_element_validate().
  $element['value']['#required'] = FALSE;
  // Create checkbox element.
  $element['na_checkbox'] = array(
    '#type' => 'checkbox',
    '#default_value' => ($element['value']['#default_value'] == $element['#na_checkbox']['na_checkbox_value']) ? $element['#na_checkbox']['na_checkbox_value'] : '',
    '#return_value' => $element['#na_checkbox']['na_checkbox_value'],
    '#attributes' => array('class' => 'na-checkbox'),
    '#title' => $element['#na_checkbox']['na_checkbox_label'],
    '#prefix' => '<div class="na-checkbox-wrapper">',
    '#suffix' => '</div>',
    '#description' => $element['value']['#description'],
  );
  // Set the description after the checkbox, instead of the text field.
  unset($element['value']['#description']);
  // Clear the textfield if the checkbox is marked.
  if ($element['na_checkbox']['#default_value'] == $element['#na_checkbox']['na_checkbox_value']) {
    $element['value']['#default_value'] = '';
  }
  drupal_add_css(drupal_get_path('module', 'na_checkbox') . '/na_checkbox.css');
  drupal_add_js(drupal_get_path('module', 'na_checkbox') . '/na_checkbox.js');
  drupal_add_js('$(document).ready(function () { na_checkbox_js("' . $element['#id'] . '");});', 'inline');
  return $element;
}

/**
 * Honor required fields, but allow for marking N/A as valid.
 */
function na_checkbox_element_validate($element, &$form_state) {
  if ($element['#required'] && ($element['#value']['value'] == '' || !isset($element['#value']['value'])) && ($element['#value']['na_checkbox'] == '' || !isset($element['#value']['na_checkbox']))) {
    form_set_error($element['#field_name'], t('!name field is required.', array('!name' => $element['#title'])));
  }
  if ($element['#value']['na_checkbox'] == $element['na_checkbox']['#return_value']) {
    form_set_value($element['value'], $element['na_checkbox']['#return_value'], $form_state);
  }
}

/**
 * Implementation of hook_theme().
 */
function na_checkbox_theme() {
  return array(
    'number' => array(
      'arguments' => array('element' => NULL),
      'function' => 'theme_na_checkbox_required',
    ),
  );
}

/**
 * Proxy theme function to display the "field required" asterisk.
 */
function theme_na_checkbox_required($element) {
  if ($element['#required']) {
    $required = !empty($element['#required']) ? '<span class="form-required" title="'. t('This field is required.') .'">*</span>' : '';
    $element['#children'] = str_replace($element['#title'] . ': ', $element['#title'] . ': ' . $required, $element['#children']);
  }
  return $element['#children'];
}
