
function na_checkbox_js(id) {
  if (Drupal.jsEnabled) {
    $(document).ready(function () {
      $("#" + id + "-na-checkbox").ready(function() { na_checkbox_toggle(id); });
      $("#" + id + "-na-checkbox").click(function() { na_checkbox_toggle(id); });
    });
  }
}

function na_checkbox_toggle(id) {
  if ($("#" + id + "-na-checkbox").attr("checked") == true) {
    $("#" + id + "-value").attr("disabled", "disabled");
    $("#" + id + "-value").val("");
  }
  else {
    $("#" + id + "-value").removeAttr("disabled");
  }
}
